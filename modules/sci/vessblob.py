import os
import io

from skimage.filters import frangi
import pydicom as dicom
import numpy as np
import math
from PIL import Image
import cv2
import argparse

import progressbar

class Candidate: 
    def __init__(self, layer, pt, size):
        self.layer = layer
        self.pt = pt
        self.size = size

    def mirror(pt, a): 
        x,y = pt
        return ((a-x)+a,y)

def display(image):
    image = np.array(image, copy=False)
    display_min = np.amin(image)
    display_max = np.amax(image)
    image.clip(display_min, display_max, out=image)
    image -= display_min
    np.floor_divide(image, (display_max - display_min + 1) / 256,
                    out=image, casting='unsafe')
    return image

def getParams():
    # Set parameters
    params = cv2.SimpleBlobDetector_Params()

    # Change thresholds
    params.minThreshold = 110
    params.maxThreshold = 255

    # Filter by size
    params.filterByColor = True
    params.blobColor = 255

    # Filter by Area.
    params.filterByArea = True
    # params.maxArea = 1000
    params.minArea = 20

    # Filter by Circularity
    params.filterByCircularity = True
    params.minCircularity = 0.4

    # Filter by Convexity
    params.filterByConvexity = True
    params.minConvexity = 0.1

    # Filter by Inertia
    params.filterByInertia = True
    params.minInertiaRatio = 0.3
    params.maxInertiaRatio = 0.9

    return params

def dist(tp1, tp2):
    """
    Calculates difference between two (x,y) tuples
    """
    x1, y1 = tp1
    x2, y2 = tp2
    return math.sqrt(((x1-y1)**2) + ((x2-y2)**2))

def scan(id, slices, out_path):
    # Create detector
    params = getParams()
    detector = cv2.SimpleBlobDetector_create(params)

    # Run in 3D (needs a lot of RAM or swapspace)
    # vesselness = frangi(slices, black_ridges=True)

    count = 0
    candidates = []

    width = slices[0].shape[0]
    height = slices[0].shape[1]

    # Run slice by slice
    print("Generating filtered scans")
    for dcmslice in progressbar.progressbar(slices):
        slicecand = []
        image = display(dcmslice)

        center = (width/2, height/2)

        image32 = dcmslice.astype(np.float32)

        img = Image.fromarray(dcmslice.astype(np.uint8))
        img.save(out_path + "/scans/" + id + "-" + str(count) + ".png")
        img.close()

        # Compute vesselness of image
        vesselness = frangi(image32, black_ridges=False)
        vesselness = display(vesselness).astype(np.uint8)

        vessimg = Image.fromarray(vesselness)
        vessimg.save(out_path + "/vesselness/" + id + "-vessels-" + str(count) + ".png")
        vessimg.close()

        # Compute the blobs in the image
        image8 = image.astype(np.uint8)
        keypoints = detector.detect(image8)

        # Create blank mask using scan dimensions
        mask = np.zeros((image8.shape[0], image8.shape[1], 3), np.uint8)
        mask[:] = (0, 0, 0)

        # Draw keypoints on mask
        fmask = cv2.drawKeypoints(mask,keypoints,None,color=(0,255,0), flags=0)

        graymask = cv2.cvtColor(fmask,cv2.COLOR_BGR2GRAY)
        ret, th = cv2.threshold(graymask, 50, 255, 0)
        contours, hierarchy = cv2.findContours(th, 2, 1)
        rep = cv2.drawContours(fmask, contours, -1, (0,255,0), 5)

        repmask = cv2.cvtColor(rep, cv2.COLOR_BGR2GRAY)
        ret, th1 = cv2.threshold(repmask, 50, 255, 0)

        # Invert vesselness to hide vessels and highlight blobs
        invessel = cv2.bitwise_not(vesselness)

        # Apply mask to vesselness filter
        res = cv2.bitwise_and(invessel, invessel, mask = th1)

        ret, resdisp = cv2.threshold(res, 190, 255, cv2.THRESH_TOZERO)
    

        blobimg = Image.fromarray(resdisp)
        blobimg.save(out_path + "/blobness/" + id + "-blobs-" + str(count) + ".png")
        blobimg.close()

        # Estimate medial area
        radius = min([width,height])/3

        # Loop through candidates
        for point in keypoints:
            # Find distance from center
            distance = dist(point.pt, center)
            if distance < radius:
                slicecand.append(Candidate(count, point.pt, point.size))
        
        candidates.append(slicecand)
        count+=1

    # Loop through candidates by slice
    count = 0
    newcand = []
    print("Filtering candidates")
    for curslice, nextslice in zip(candidates, candidates[1:]):
        # Check against other candidates
        for c in curslice:
            cx, cy = c.pt
            mirrorcoord = Candidate.mirror(c.pt, width/2)
            for o in [o for o in curslice if o != c]:
                distance = dist(o.pt, mirrorcoord)
                if distance < c.size*4:
                    # print(c.layer,o.pt,c.pt, "Roughly opposite")
                    continue
                    
            # Check against next layer
            for n in nextslice:
                distance = dist(c.pt, n.pt)
                if distance < c.size:
                    # print(c.pt, n.pt, "They touching")
                    continue
                else:
                    # print(c.pt, n.pt, "Socially distancing")
                    continue

        count+=1