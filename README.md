# Cerebral Aneurysm Detection
_Individual Project for 3rd year of BSc Applied Computer Science at the University of Leeds._

This project follows the research and implementation of Automatic Cerebral Aneurysm Detection in Multimodal Angiographic Images. The goal of the project is to assist in the automated process of diagnosis and treatment of aneurysms.

This software is designed to be able to analyse multiple types of scans and is not specific to one given type.
Because of this, there may be imperfections to the approach.

This software is in no way a replacement for a medical professional, and one should always be consulted where possible.

The scans used for testing and the development of the models are anonymised and have been licensed for academic and research purposes and are in no way available for public use.

## Project Structure
This project contains multiple files that serve different purposes.

### Data
The data folder is where all of the scans are stored before and after processing.
At various stages of the process, files are saved along the way to allow for the system to be modular.
The files in the MRA and 3DRA folders are the raw DICOM data that needs to be processed.
This DICOM data is read and is then turned into a 3D numpy array to allow for other tools to process the data n their native fashion as not many tools are built to use DICOM data.
Once the data has been converted into a 3D numpy array, it is saved in a numpy file in the data folder with it's designated ID.
From here, the data can now be processed using python tools in their native format.
When processing the numpy arrays, a new folder for each file is made to allow for different pieces of data to be saved in the same place.
Each folder is named as the id for the array and contains two folders, the vesselness folder and the blobness folder.
The project uses these folders to save the slides that have been processed with their respective filters.

### Scripts

- aneurysm.py - This is the main script for the project. It takes two arguements in order to function.
    - ID: This is the ID for the patient, this will be what the array will be saved as so it can be loaded again as well as what the directories and files will be named.
    - DICOM: This is for when you wish to create a new array with a new patient. This takes the folder path from the project root to the folder containing all of the data.

#### dicom
 - example/VR_dicom.py - This script is for visualising DICOM files as vtk
 - example/resample.py - This script is for converting DICOM to a numpy array file

#### sci
 - sci/vessblob.py - vessblob.py uses filters and detectors on the numpy array file _slice by slice_ and saves them to their respective folders.

#### 3dFrangi
This is where I attempted to use the full array instead of slice by slice using different filters. Due to the higher system requirements, my laptop is not able to load and process the array all at once.
