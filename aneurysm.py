import argparse
from os import path, makedirs
import numpy as np

import modules.dicom.resample as resample
import modules.sci.vessblob as vessblob

parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dicom", help = "Data Path")
mand = parser.add_argument_group("Mandatory arguments")
mand.add_argument("-id", help = "Patient ID", required=True)
args = parser.parse_args()

id=args.id
np_path = working_path = "data/npdata/"
folder_path = np_path + id
npdatapath = np_path + "fullimages_%s.npy" % (id)

if not path.isfile(npdatapath) or args.dicom:
    if not args.dicom:
        print("DICOM path required")
        exit()
    data_path = args.dicom

    patient = resample.load_scan(data_path)
    imgs = resample.get_pixels_hu(patient)

    print("Shape before resampling\t", imgs.shape)
    imgs_after_resamp, spacing = resample.resample(imgs, patient, [1,1,1])
    print("Shape after resampling\t", imgs_after_resamp.shape)

    np.save(npdatapath, imgs)
    print(npdatapath)

scan = np.load(npdatapath)
makedirs(folder_path, exist_ok=True)
makedirs(folder_path+"/scans", exist_ok=True)
makedirs(folder_path+"/vesselness", exist_ok=True)
makedirs(folder_path+"/blobness", exist_ok=True)

vessblob.scan(id, scan, folder_path)